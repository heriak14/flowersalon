package com.epam.model.interfaces;

public interface Observable {
    void update(Bouquet bouquet);
}
