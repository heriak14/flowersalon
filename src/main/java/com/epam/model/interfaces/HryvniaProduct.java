package com.epam.model.interfaces;

import com.epam.model.currencies.UAHryvnia;

public interface HryvniaProduct {
    UAHryvnia getPrice();
}
