package com.epam.model.interfaces;

import java.util.ResourceBundle;

public interface Bouquet{
    ResourceBundle bundle = ResourceBundle.getBundle("config");

    void addFlowers(int number);

    void cutFlowers(double length);

    void bind();

    void wrap();

    String getStatus();
}
