package com.epam.model.interfaces;

import com.epam.model.currencies.PLZloty;

public interface ZlotyProduct {
    PLZloty getPrice();
}
