package com.epam.model.enums;

import com.epam.model.currencies.UAHryvnia;

import java.util.ResourceBundle;

public enum Event {
    WEDDING(new UAHryvnia(Integer.parseInt(ResourceBundle.getBundle("config")
            .getString("wedding")), 0), "WHITE"),
    BIRTHDAY(new UAHryvnia(Integer.parseInt(ResourceBundle.getBundle("config")
            .getString("funerals")), 0), "YELLOW"),
    FUNERALS(new UAHryvnia(Integer.parseInt(ResourceBundle.getBundle("config")
            .getString("valentine")), 0), "BLACK"),
    VALENTINE(new UAHryvnia(Integer.parseInt(ResourceBundle.getBundle("config")
            .getString("birthday")), 0), "RED");

    UAHryvnia price;
    String colorStyle;

    Event(UAHryvnia price, String color) {
        this.price = price;
        colorStyle = color;
    }

    public UAHryvnia getPrice() {
        return price;
    }

    public String getColor() {
        return colorStyle;
    }

    @Override
    public String toString() {
        return super.toString() + "(everything done in " + colorStyle + " colors)";
    }
}
