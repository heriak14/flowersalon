package com.epam.model.enums;

import com.epam.model.currencies.UAHryvnia;

public enum Card {
    GOLD(20,0),
    VIP(30,0),
    SILVER(10,0),
    ORDINARY(0, 0);

    UAHryvnia discount;

    Card(int bill, int coin){
        discount = new UAHryvnia(bill,coin);
    }

    public UAHryvnia getDiscount() {
        return discount;
    }
}
