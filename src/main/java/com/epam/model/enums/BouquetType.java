package com.epam.model.enums;

public enum BouquetType {
    TULIP, ROSE, DAHLIA, DAFFODIL, CHAMOMILE
}
