package com.epam.model.bouquets;

import com.epam.model.Client;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.enums.City;
import com.epam.model.interfaces.ZlotyProduct;
import com.epam.model.currencies.PLZloty;

public class TulipsBouquet implements Bouquet, ZlotyProduct {

    private int tulipsNumber;
    private PLZloty price;
    private double length;
    private String status;
    private City flowerGrowingCity;
    private Client client;

    public TulipsBouquet(City flowerGrowingCity, Client client) {
        this.flowerGrowingCity = flowerGrowingCity;
        this.client = client;
        double pr = Double.parseDouble(bundle.getString("TulipPrice"));
        price = new PLZloty((int) pr, (int) ((pr - (int) pr)*100));
    }

    @Override
    public void addFlowers(int number) {
        tulipsNumber = number;
        status = "Adding " + tulipsNumber + " tulips from " + flowerGrowingCity + " to bouquet...";
        client.update(this);
    }

    @Override
    public void cutFlowers(double length) {
        this.length = length;
        status = "Cutting " + tulipsNumber + " tulips to " + length + " cm. length...";
        client.update(this);
    }

    @Override
    public void bind() {
        status = "Binding " + tulipsNumber + " tulips of " + length + " cm. length to bouquet...";
        client.update(this);
    }

    @Override
    public void wrap() {
        status = "Wrapping bouquet...";
        client.update(this);
        status = "Your bouquet is ready!...";
        client.update(this);
    }

    @Override
    public PLZloty getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Tulip bouquet{ price: " + price + "; length: " + length + "; status" + status + "}\n";
    }
}
