package com.epam.model.bouquets;

import com.epam.model.Client;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.enums.City;
import com.epam.model.interfaces.HryvniaProduct;
import com.epam.model.currencies.UAHryvnia;

public class ChamomileBouquet implements Bouquet, HryvniaProduct {

    private int chamomileNumber;
    private UAHryvnia price;
    private double length;
    private String status;
    private City flowerGrowingCity;
    private Client client;

    public ChamomileBouquet(City flowerGrowingCity, Client client) {
        this.flowerGrowingCity = flowerGrowingCity;
        this.client = client;
        double pr = Double.parseDouble(bundle.getString("ChamomilePrice"));
        price = new UAHryvnia((int) pr, (int) ((pr - (int) pr)*100));
    }

    @Override
    public void addFlowers(int number) {
        chamomileNumber = number;
        status = "Adding " + chamomileNumber + " chamomiles from " + flowerGrowingCity + " to bouquet...";
        client.update(this);
    }

    @Override
    public void cutFlowers(double length) {
        this.length = length;
        status = "Cutting " + chamomileNumber + " chamomiles to " + length + " cm. length...";
        client.update(this);
    }

    @Override
    public void bind() {
        status = "Binding " + chamomileNumber + " chamomiles of " + length + " cm. length to bouquet...";
        client.update(this);
    }

    @Override
    public void wrap() {
        status = "Wrapping bouquet...";
        client.update(this);
        status = "Your bouquet is ready!...";
        client.update(this);
    }

    @Override
    public UAHryvnia getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Chamomile bouquet{ price: " + price + "; length: " + length + "; status" + status + "}\n";
    }
}
