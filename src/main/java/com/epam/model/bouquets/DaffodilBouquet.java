package com.epam.model.bouquets;

import com.epam.model.Client;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.enums.City;
import com.epam.model.interfaces.HryvniaProduct;
import com.epam.model.currencies.UAHryvnia;

public class DaffodilBouquet implements Bouquet, HryvniaProduct {

    private int daffodilNumber;
    private UAHryvnia price;
    private double length;
    private String status;
    private City flowerGrowingCity;
    private Client client;

    public DaffodilBouquet(City flowerGrowingCity, Client client) {
        this.flowerGrowingCity = flowerGrowingCity;
        this.client = client;
        double pr = Double.parseDouble(bundle.getString("DaffodiPrice"));
        price = new UAHryvnia((int) pr, (int) ((pr - (int) pr)*100));
    }

    @Override
    public void addFlowers(int number) {
        daffodilNumber = number;
        status = "Adding " + daffodilNumber + " daffodils from " + flowerGrowingCity + " to bouquet...";
        client.update(this);
    }

    @Override
    public void cutFlowers(double length) {
        this.length = length;
        status = "Cutting " + daffodilNumber + " daffodils to " + length + " cm. length...";
        client.update(this);
    }

    @Override
    public void bind() {
        status = "Binding " + daffodilNumber + " daffodils of " + length + " cm. length to bouquet...";
        client.update(this);
    }

    @Override
    public void wrap() {
        status = "Wrapping bouquet...";
        client.update(this);
        status = "Your bouquet is ready!...";
        client.update(this);
    }

    @Override
    public UAHryvnia getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Daffodil bouquet{ price: " + price + "; length: " + length + "; status" + status + "}\n";
    }
}
