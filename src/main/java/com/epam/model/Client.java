package com.epam.model;

import com.epam.model.currencies.UAHryvnia;
import com.epam.model.enums.Card;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.interfaces.HryvniaProduct;
import com.epam.model.interfaces.Observable;
import com.epam.view.View;

import java.util.LinkedList;
import java.util.List;

public class Client implements Observable {
    private String name;
    private UAHryvnia money;
    private List<? super HryvniaProduct> bouquets;
    private Card card;

    public Client(String name, UAHryvnia hryvniaBalance) {
        this.name = name;
        money = hryvniaBalance;
        bouquets = new LinkedList<>();
    }

    public boolean buyBouquet(HryvniaProduct product) {
        boolean result = money.subtract(new UAHryvnia(product.getPrice().getHryvnia(),
                product.getPrice().getKopiyka()));
        if (result) {
            bouquets.add(product);
        }
        return result;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public List<? super HryvniaProduct> getBouquets() {
        return bouquets;
    }

    public Card getCard(){
        return card;
    }

    @Override
    public String toString() {
        return "Client{" + "name: " + name  + ", balance :" + money + "}";
    }

    @Override
    public void update(Bouquet bouquet) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            View.showMessage(e.getMessage());
        }
        View.showMessage(bouquet.getStatus());
    }
}
