package com.epam.model.currencies;

public class UAHryvnia{
    private int hryvnia;
    private int kopiyka;

    public UAHryvnia(int hryvnia, int kopiyka) {
        if (hryvnia < 0 || kopiyka < 0) {
            throw new IllegalArgumentException("Hryvnia or Kopiyka has an invalid value");
        }
        this.hryvnia = hryvnia;
        if(kopiyka >= 100){
            this.hryvnia += kopiyka / 100;
            this.kopiyka = kopiyka % 100;
        } else {
            this.kopiyka = kopiyka;
        }
    }

    public boolean subtract(UAHryvnia value) {
        int temp = (hryvnia * 100 + kopiyka) - (value.getHryvnia() * 100 + value.getKopiyka());
        if (temp < 0) {
            return false;
        } else {
            hryvnia = temp / 100;
            kopiyka = temp % 100;
            return true;
        }
    }

    public boolean add(UAHryvnia value){
        int temp = (hryvnia * 100 + kopiyka) + (value.getHryvnia() * 100 + value.getKopiyka());
        if (temp < 0) {
            return false;
        } else {
            hryvnia = temp / 100;
            kopiyka = temp % 100;
            return true;
        }
    }

    public int getHryvnia() {
        return hryvnia;
    }

    public int getKopiyka() {
        return kopiyka;
    }

    @Override
    public String toString() {
        return "" + hryvnia + "." + kopiyka + " UAH";
    }
}
