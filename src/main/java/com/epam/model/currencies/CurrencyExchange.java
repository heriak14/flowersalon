package com.epam.model.currencies;

import com.epam.model.interfaces.HryvniaProduct;
import com.epam.model.interfaces.ZlotyProduct;

public class CurrencyExchange implements HryvniaProduct {
    private double hryvniaRate;
    private ZlotyProduct product;

    public CurrencyExchange(double hryvniaRate, ZlotyProduct product) {
        this.hryvniaRate = hryvniaRate;
        this.product = product;
    }

    @Override
    public UAHryvnia getPrice() {
        PLZloty zlotyPrice = product.getPrice();
        int groszPrice = zlotyPrice.getZloty() * 100 + zlotyPrice.getGrosz();
        int priceKopiyka = (int) (groszPrice * hryvniaRate);
        return new UAHryvnia(priceKopiyka / 100, priceKopiyka % 100);
    }
}
