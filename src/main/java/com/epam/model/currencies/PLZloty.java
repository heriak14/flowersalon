package com.epam.model.currencies;

public class PLZloty{
    private int zloty;
    private int grosz;

    public PLZloty(int zloty, int grosz) {
        if (zloty < 0 || grosz < 0) {
            throw new IllegalArgumentException("Zloty or Grosz has an invalid value");
        }
        this.zloty = zloty;
        if(grosz >= 100){
            this.zloty += grosz / 100;
            this.grosz = grosz % 100;
        } else {
            this.grosz = grosz;
        }
    }

    public boolean subtract(PLZloty value) {
        int temp = (zloty * 100 + grosz) - (value.getZloty() * 100 + value.getGrosz());
        if (temp < 0) {
            return false;
        } else {
            zloty = temp / 100;
            grosz = temp % 100;
            return true;
        }
    }

    public boolean add(PLZloty value){
        int temp = (zloty * 100 + grosz) + (value.getZloty() * 100 + value.getGrosz());
        if (temp < 0) {
            return false;
        } else {
            zloty = temp / 100;
            grosz = temp % 100;
            return true;
        }
    }

    int getZloty() {
        return zloty;
    }

    int getGrosz() {
        return grosz;
    }

    @Override
    public String toString() {
        return "" + zloty + "." + grosz + " PLZ";
    }
}
