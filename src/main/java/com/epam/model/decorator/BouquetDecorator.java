package com.epam.model.decorator;

import com.epam.model.interfaces.Bouquet;
import com.epam.model.interfaces.HryvniaProduct;
import com.epam.model.interfaces.ZlotyProduct;
import com.epam.model.currencies.CurrencyExchange;
import com.epam.model.currencies.UAHryvnia;

import java.util.Optional;

public class BouquetDecorator implements Bouquet, HryvniaProduct {
    private Optional<Bouquet> bouquet;
    private String name;
    private UAHryvnia generalPrice;
    private String additionalComponent = "";
    private String status;

    protected BouquetDecorator() {
        name = "";
        generalPrice = new UAHryvnia(0, 0);
        additionalComponent = "";
    }

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = Optional.ofNullable(bouquet);
        this.status = bouquet.getStatus();
        if (bouquet instanceof HryvniaProduct) {
            generalPrice.add(((HryvniaProduct) bouquet).getPrice());
        } else {
            HryvniaProduct product = new CurrencyExchange(
                    Double.parseDouble(bundle.getString("plzInGrn")), ((ZlotyProduct) bouquet));
            generalPrice.add(product.getPrice());
        }
        if (bouquet instanceof BouquetDecorator) {
            setAdditionalComponent(((BouquetDecorator) bouquet).getAdditionalComponent());
            setName(((BouquetDecorator) bouquet).getName());
        } else {
            setName(bouquet.toString());
        }
    }

    protected void setAdditionalPrice(UAHryvnia additionalPrice, boolean add) {
        if (add) {
            generalPrice.add(additionalPrice);
        } else {
            generalPrice.subtract(additionalPrice);
        }
    }

    protected void setAdditionalComponent(String additionalComponent) {
        this.additionalComponent += additionalComponent + ", ";
        status = "Adding additional component: " + additionalComponent;
    }

    private void setName(String name) {
        this.name = name;
    }

    private String getName() {
        return name;
    }

    private String getAdditionalComponent() {
        return additionalComponent;
    }

    @Override
    public void addFlowers(int number) {
        bouquet.orElseThrow(IllegalArgumentException::new).addFlowers(number);
    }

    @Override
    public void cutFlowers(double length) {
        bouquet.orElseThrow(IllegalArgumentException::new).cutFlowers(length);
    }

    @Override
    public void bind() {
        bouquet.orElseThrow(IllegalArgumentException::new).bind();
    }

    @Override
    public void wrap() {
        bouquet.orElseThrow(IllegalArgumentException::new).wrap();
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return name + "Additional work: " + additionalComponent
                + " General price: " + generalPrice + "\n";
    }

    @Override
    public UAHryvnia getPrice() {
        return generalPrice;
    }
}
