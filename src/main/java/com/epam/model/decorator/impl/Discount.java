package com.epam.model.decorator.impl;

import com.epam.model.currencies.UAHryvnia;
import com.epam.model.decorator.BouquetDecorator;
import com.epam.model.enums.Card;

public class Discount extends BouquetDecorator {
    private UAHryvnia value;

    public Discount(Card card) {
        value = card.getDiscount();
    }

    public void useDiscount(){
        setAdditionalComponent("Discount :" + value);
        setAdditionalPrice(value, false);
    }
}
