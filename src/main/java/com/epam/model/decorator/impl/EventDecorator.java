package com.epam.model.decorator.impl;

import com.epam.model.decorator.BouquetDecorator;
import com.epam.model.enums.Event;

public class EventDecorator extends BouquetDecorator {
    public EventDecorator(Event eventType) {
        setAdditionalComponent("Event " + eventType + ": " + eventType.getPrice());
        setAdditionalPrice(eventType.getPrice(), true);
    }
}
