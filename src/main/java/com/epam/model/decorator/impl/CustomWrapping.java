package com.epam.model.decorator.impl;

import com.epam.model.currencies.UAHryvnia;
import com.epam.model.decorator.BouquetDecorator;

public class CustomWrapping extends BouquetDecorator {
    public CustomWrapping() {
        double price = Double.parseDouble(bundle.getString("WrappingPrice"));
        UAHryvnia grn = new UAHryvnia((int) price, (int) ((price - (int) price) * 100));
        setAdditionalComponent("CustomWrapping :" + grn);
        setAdditionalPrice(grn, true);
    }
}
