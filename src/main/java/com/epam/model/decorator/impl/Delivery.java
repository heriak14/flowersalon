package com.epam.model.decorator.impl;

import com.epam.model.currencies.UAHryvnia;
import com.epam.model.decorator.BouquetDecorator;

public class Delivery extends BouquetDecorator {
    public Delivery() {
        double price = Double.valueOf(bundle.getString("deliveryPrice"));
        UAHryvnia grn = new UAHryvnia((int) price, (int) ((price - (int) price) * 100));
        setAdditionalComponent("Delivery :" + grn);
        setAdditionalPrice(grn, true);
    }
}
