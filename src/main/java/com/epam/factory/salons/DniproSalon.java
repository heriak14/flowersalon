package com.epam.factory.salons;

import com.epam.factory.FlowerSalon;
import com.epam.model.Client;
import com.epam.model.bouquets.DahliaBouquet;
import com.epam.model.bouquets.RosesBouquet;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.enums.BouquetType;
import com.epam.model.enums.City;
import com.epam.model.bouquets.ChamomileBouquet;
import com.epam.model.bouquets.DaffodilBouquet;

public class DniproSalon extends FlowerSalon {
    @Override
    protected Bouquet getBouquet(BouquetType type, Client client) {
        if (type == BouquetType.CHAMOMILE) {
            return new ChamomileBouquet(City.DNIPRO, client);
        } else if (type == BouquetType.DAFFODIL) {
            return new DaffodilBouquet(City.LVIV, client);
        } else if (type == BouquetType.DAHLIA) {
            return new DahliaBouquet(City.KRAKOW, client);
        } else if (type == BouquetType.ROSE) {
            return new RosesBouquet(City.DNIPRO, client);
        } else if (type == BouquetType.TULIP) {
            return new DaffodilBouquet(City.WARSAWA, client);
        } else {
            return null;
        }
    }
}
