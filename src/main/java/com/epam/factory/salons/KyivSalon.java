package com.epam.factory.salons;

import com.epam.factory.FlowerSalon;
import com.epam.model.Client;
import com.epam.model.bouquets.*;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.enums.BouquetType;
import com.epam.model.enums.City;

public class KyivSalon extends FlowerSalon {
    @Override
    protected Bouquet getBouquet(BouquetType type, Client client) {
        if (type == BouquetType.CHAMOMILE) {
            return new ChamomileBouquet(City.KYIV, client);
        } else if (type == BouquetType.DAFFODIL) {
            return new DaffodilBouquet(City.DNIPRO, client);
        } else if (type == BouquetType.DAHLIA) {
            return new DahliaBouquet(City.WROCLAW, client);
        } else if (type == BouquetType.ROSE) {
            return new RosesBouquet(City.LVIV, client);
        } else if (type == BouquetType.TULIP) {
            return new TulipsBouquet(City.WROCLAW, client);
        } else {
            return null;
        }
    }
}
