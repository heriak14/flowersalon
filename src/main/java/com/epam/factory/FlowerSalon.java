package com.epam.factory;

import com.epam.model.Client;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.enums.BouquetType;

public abstract class FlowerSalon {

    public Bouquet makeBouquet(BouquetType type, int flowersNum, double length, Client client) {
        Bouquet bouquet = getBouquet(type, client);
        bouquet.addFlowers(flowersNum);
        bouquet.cutFlowers(length);
        bouquet.bind();
        bouquet.wrap();
        return bouquet;
    }

    protected abstract Bouquet getBouquet(BouquetType type, Client client);
}
