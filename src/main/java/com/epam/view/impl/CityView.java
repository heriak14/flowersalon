package com.epam.view.impl;

import com.epam.factory.FlowerSalon;
import com.epam.factory.salons.DniproSalon;
import com.epam.factory.salons.KyivSalon;
import com.epam.factory.salons.LvivSalon;
import com.epam.model.Client;
import com.epam.model.currencies.UAHryvnia;
import com.epam.model.enums.Card;
import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.View;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class CityView implements View {
    private static Map<String, String> cityMenu;
    private static Map<String, Printable> cityMethods;
    private static FlowerSalon salon;
    private Client client;

    public CityView() {
        cityMenu = new Menu("menu/city_menu").getMenu();
        registerClient();
        setMethods();
    }

    private void registerClient() {
        LOGGER.info("Enter client name: ");
        String name = SCANNER.nextLine();
        LOGGER.info("Enter balance: ");
        double balance = Double.parseDouble(SCANNER.nextLine());
        client = new Client(name, new UAHryvnia((int) balance, (int)(balance - (int) balance) * 100));
        client.setCard(chooseCard());
    }

    private Card chooseCard() {
        LOGGER.info("Choose your card:\n");
        for (Card c: Card.values()) {
            LOGGER.info(c.ordinal() + " - " + c + "\n");
        }
        LOGGER.info("Enter card number: ");
        int number = SCANNER.nextInt();
        SCANNER.nextLine();
        return Arrays.stream(Card.values())
                .filter(c -> c.ordinal() == number)
                .findAny()
                .orElse(Card.ORDINARY);
    }

    @Override
    public void setMethods() {
        cityMethods = new LinkedHashMap<>();
        cityMethods.put("1", this::chooseLviv);
        cityMethods.put("2", this::chooseDnipro);
        cityMethods.put("3", this::chooseKyiv);
        cityMethods.put("q", this::quit);
    }

    @Override
    public void printMenu() {
        LOGGER.info("_______________________"
                + "CITIES______________________\n");
        for (Map.Entry entry : cityMenu.entrySet()) {
            LOGGER.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        LOGGER.info("____________________________________________\n");
    }

    private void chooseLviv() {
        salon = new LvivSalon();
        new BouquetView(client, salon).show();
    }

    private void chooseDnipro() {
        salon = new DniproSalon();
        new BouquetView(client, salon).show();
    }

    private void chooseKyiv() {
        salon = new KyivSalon();
        new BouquetView(client, salon).show();
    }

    private void quit(){
    }

    @Override
    public void show() {
        String option;
        do {
            printMenu();
            LOGGER.info("Choose your city: ");
            option = SCANNER.nextLine().toLowerCase().trim();
            if(cityMethods.containsKey(option)){
                cityMethods.get(option).print();
            }else {
                LOGGER.info("Wrong input! Try again.");
            }
        } while (!option.equals("q"));
    }
}
