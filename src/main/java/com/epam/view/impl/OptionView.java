package com.epam.view.impl;

import com.epam.model.Client;
import com.epam.model.currencies.CurrencyExchange;
import com.epam.model.decorator.BouquetDecorator;
import com.epam.model.decorator.impl.CustomWrapping;
import com.epam.model.decorator.impl.Delivery;
import com.epam.model.decorator.impl.Discount;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.interfaces.HryvniaProduct;
import com.epam.model.interfaces.ZlotyProduct;
import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.View;

import java.util.LinkedHashMap;
import java.util.Map;

public class OptionView implements View {
    private static Map<String, String> optionMenu;
    private static Map<String, Printable> optionMethods;
    private Bouquet currentBouquet;
    private Client client;

    OptionView(Client client, Bouquet bouquet) {
        this.client = client;
        this.currentBouquet = bouquet;
        optionMenu = new Menu("menu/options_menu").getMenu();
        setMethods();
    }

    @Override
    public void setMethods() {
        optionMethods = new LinkedHashMap<>();
        optionMethods.put("1", this::buyBouquet);
        optionMethods.put("2", this::makeCustomWrap);
        optionMethods.put("3", this::makeEventDecoration);
        optionMethods.put("4", this::getDiscount);
        optionMethods.put("5", this::makeDelivery);
        optionMethods.put("q", this::quit);
    }

    private void buyBouquet() {
        if (currentBouquet instanceof ZlotyProduct) {
            CurrencyExchange exchanger = new CurrencyExchange(
                    Double.parseDouble(Bouquet.bundle.getString("plzInGrn")), (ZlotyProduct) currentBouquet);
            client.buyBouquet(exchanger);
        } else {
            client.buyBouquet((HryvniaProduct) currentBouquet);
        }
        LOGGER.info("\nPurchase successful!...\n");
        LOGGER.info(client + "\n");
    }

    private void makeEventDecoration() {
        new EventView(client, currentBouquet).show();
    }

    private void makeCustomWrap() {
        BouquetDecorator wrapper = new CustomWrapping();
        wrapper.setBouquet(currentBouquet);
        currentBouquet = wrapper;
        LOGGER.info(currentBouquet + "\n");
    }

    private void makeDelivery() {
        BouquetDecorator delivery = new Delivery();
        delivery.setBouquet(currentBouquet);
        currentBouquet = delivery;
        LOGGER.info(currentBouquet + "\n");
    }

    private void getDiscount() {
        Discount discount = new Discount(client.getCard());
        discount.setBouquet(currentBouquet);
        discount.useDiscount();
        currentBouquet = discount;
        LOGGER.info(currentBouquet + "\n");
    }

    private void quit(){
    }

    @Override
    public void printMenu() {
        LOGGER.info("_______________________"
                + "OPTIONS______________________\n");
        for (Map.Entry entry : optionMenu.entrySet()) {
            LOGGER.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        LOGGER.info("____________________________________________\n");
    }

    @Override
    public void show() {
        String option;
        do {
            printMenu();
            LOGGER.info("Choose one option: ");
            option = SCANNER.nextLine().toLowerCase().trim();
            if(optionMethods.containsKey(option)){
                optionMethods.get(option).print();
            }else {
                LOGGER.info("Wrong input! Try again.");
            }
        } while (!option.equals("q"));
    }
}
