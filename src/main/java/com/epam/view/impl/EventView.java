package com.epam.view.impl;

import com.epam.model.Client;
import com.epam.model.currencies.CurrencyExchange;
import com.epam.model.decorator.BouquetDecorator;
import com.epam.model.decorator.impl.EventDecorator;
import com.epam.model.enums.Event;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.interfaces.HryvniaProduct;
import com.epam.model.interfaces.ZlotyProduct;
import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.View;

import java.util.LinkedHashMap;
import java.util.Map;

public class EventView implements View {
    private static Map<String, String> eventMenu;
    private static Map<String, Printable> eventMethods;
    private Bouquet currentBouquet;
    private Client client;

    EventView(Client client, Bouquet bouquet) {
        this.client = client;
        this.currentBouquet = bouquet;
        eventMenu = new Menu("menu/decoration_menu").getMenu();
        setMethods();
    }

    @Override
    public void setMethods() {
        eventMethods = new LinkedHashMap<>();
        eventMethods.put("1", this::buyBouquet);
        eventMethods.put("2", this::makeWeddingDecoration);
        eventMethods.put("3", this::makeBirthdayDecoration);
        eventMethods.put("4", this::makeFuneralDecoration);
        eventMethods.put("5", this::makeValentinesDecoration);
        eventMethods.put("q", this::quit);
    }

    private void buyBouquet() {
        if (currentBouquet instanceof ZlotyProduct) {
            CurrencyExchange exchanger = new CurrencyExchange(
                    Double.parseDouble(Bouquet.bundle.getString("plzInGrn")), (ZlotyProduct) currentBouquet);
            client.buyBouquet(exchanger);
        } else {
            client.buyBouquet((HryvniaProduct) currentBouquet);
        }
        LOGGER.info("\nPurchase successful!...\n");
        LOGGER.info(client + "\n");
    }

    private void makeWeddingDecoration() {
        BouquetDecorator wedding = new EventDecorator(Event.WEDDING);
        wedding.setBouquet(currentBouquet);
        currentBouquet = wedding;
        LOGGER.info(currentBouquet + "\n");
    }

    private void makeBirthdayDecoration() {
        BouquetDecorator birthday = new EventDecorator(Event.BIRTHDAY);
        birthday.setBouquet(currentBouquet);
        currentBouquet = birthday;
        LOGGER.info(currentBouquet + "\n");
    }

    private void makeFuneralDecoration() {
        BouquetDecorator funeral = new EventDecorator(Event.FUNERALS);
        funeral.setBouquet(currentBouquet);
        currentBouquet = funeral;
        LOGGER.info(currentBouquet + "\n");
    }

    private void makeValentinesDecoration() {
        BouquetDecorator valentines = new EventDecorator(Event.VALENTINE);
        valentines.setBouquet(currentBouquet);
        currentBouquet = valentines;
        LOGGER.info(currentBouquet + "\n");
    }

    private void quit(){
    }

    @Override
    public void printMenu() {
        LOGGER.info("_______________________"
                + "EVENTS______________________\n");
        for (Map.Entry entry : eventMenu.entrySet()) {
            LOGGER.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        LOGGER.info("____________________________________________\n");
    }

    @Override
    public void show() {
        String option;
        do {
            printMenu();
            LOGGER.info("Choose event to decorate: ");
            option = SCANNER.nextLine().toLowerCase().trim();
            if(eventMethods.containsKey(option)){
                eventMethods.get(option).print();
            }else {
                LOGGER.info("Wrong input! Try again.");
            }
        } while (!option.equals("q"));
    }
}
