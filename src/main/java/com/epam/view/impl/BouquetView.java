package com.epam.view.impl;

import com.epam.factory.FlowerSalon;
import com.epam.model.Client;
import com.epam.model.enums.BouquetType;
import com.epam.model.interfaces.Bouquet;
import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.View;

import java.util.LinkedHashMap;
import java.util.Map;

public class BouquetView implements View {
    private static Map<String, String> bouquetMenu;
    private static Map<String, Printable> bouquetMethods;
    private FlowerSalon salon;
    private Bouquet currentBouquet;
    private Client client;

    BouquetView(Client client, FlowerSalon salon) {
        this.client = client;
        this.salon = salon;
        bouquetMenu = new Menu("menu/bouquet_menu").getMenu();
        setMethods();
    }

    @Override
    public void setMethods() {
        bouquetMethods = new LinkedHashMap<>();
        bouquetMethods.put("1", this::chooseChamomile);
        bouquetMethods.put("2", this::chooseDaffodil);
        bouquetMethods.put("3", this::chooseRose);
        bouquetMethods.put("4", this::chooseDahlia);
        bouquetMethods.put("5", this::chooseTulip);
        bouquetMethods.put("q", this::quit);
    }

    @Override
    public void printMenu() {
        LOGGER.info("_______________________"
                + "BOUQUETS______________________\n");
        for (Map.Entry entry : bouquetMenu.entrySet()) {
            LOGGER.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        LOGGER.info("____________________________________________\n");
    }

    private void chooseChamomile() {
        double[] details = getFlowerDetails();
        currentBouquet = salon.makeBouquet(BouquetType.CHAMOMILE, (int) details[0], details[1], client);
        LOGGER.info(currentBouquet + "\n");
        new OptionView(client, currentBouquet).show();
    }

    private void chooseDaffodil() {
        double[] details = getFlowerDetails();
        currentBouquet = salon.makeBouquet(BouquetType.DAFFODIL, (int) details[0], details[1], client);
        LOGGER.info(currentBouquet + "\n");
        new OptionView(client, currentBouquet).show();
    }

    private void chooseRose() {
        double[] details = getFlowerDetails();
        currentBouquet = salon.makeBouquet(BouquetType.ROSE, (int) details[0], details[1], client);
        LOGGER.info(currentBouquet + "\n");
        new OptionView(client, currentBouquet).show();
    }

    private void chooseDahlia() {
        double[] details = getFlowerDetails();
        currentBouquet = salon.makeBouquet(BouquetType.DAHLIA, (int) details[0], details[1], client);
        LOGGER.info(currentBouquet + "\n");
        new OptionView(client, currentBouquet).show();
    }

    private void chooseTulip() {
        double[] details = getFlowerDetails();
        currentBouquet = salon.makeBouquet(BouquetType.CHAMOMILE, (int) details[0], details[1], client);
        LOGGER.info(currentBouquet + "\n");
        new OptionView(client, currentBouquet).show();
    }

    private double[] getFlowerDetails() {
        LOGGER.info("Enter number of flowers: ");
        int flowersNum = SCANNER.nextInt();
        LOGGER.info("Enter length if each flower: ");
        SCANNER.nextLine();
        double length = Double.parseDouble(SCANNER.nextLine());
        return new double[]{flowersNum, length};
    }

    private void quit(){
    }

    @Override
    public void show() {
        String option;
        do {
            printMenu();
            LOGGER.info("Choose bouquet: ");
            option = SCANNER.nextLine().toLowerCase().trim();
            if(bouquetMethods.containsKey(option)){
                bouquetMethods.get(option).print();
            }else {
                LOGGER.info("Wrong input! Try again.");
            }
        } while (!option.equals("q"));
    }
}
