package com.epam;

import com.epam.factory.FlowerSalon;
import com.epam.factory.salons.LvivSalon;
import com.epam.model.enums.BouquetType;
import com.epam.model.interfaces.Bouquet;
import com.epam.model.Client;
import com.epam.model.bouquets.RosesBouquet;
import com.epam.model.currencies.UAHryvnia;
import com.epam.model.decorator.BouquetDecorator;
import com.epam.model.decorator.impl.CustomWrapping;
import com.epam.model.decorator.impl.Delivery;
import com.epam.model.decorator.impl.Discount;
import com.epam.model.decorator.impl.EventDecorator;
import com.epam.model.enums.Card;
import com.epam.model.enums.City;
import com.epam.model.enums.Event;
import com.epam.view.impl.CityView;

public class Main {
    public static void main(String[] args) {
        new CityView().show();
    }
}
